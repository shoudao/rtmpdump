LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
PROJECT_ROOT := $(realpath $(LOCAL_PATH)/..)
LOCAL_MODULE := librtmp
LOCAL_SRC_FILES := \
    $(PROJECT_ROOT)/librtmp/amf.c \
    $(PROJECT_ROOT)/librtmp/hashswf.c \
    $(PROJECT_ROOT)/librtmp/log.c \
    $(PROJECT_ROOT)/librtmp/parseurl.c \
    $(PROJECT_ROOT)/librtmp/rtmp.c \

LOCAL_C_INCLUDES := $(PROJECT_ROOT)/librtmp
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib -llog
LOCAL_CFLAGS := -Wall -O2 -DSYS=posix -DNO_CRYPTO


include $(BUILD_STATIC_LIBRARY)