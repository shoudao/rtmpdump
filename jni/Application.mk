APP_PLATFORM := android-9
APP_ABI := arm64-v8a armeabi-v7a
APP_OPTIM := release
NDK_TOOLCHAIN_VERSION := 4.9
APP_STL := gnustl_static
APP_BUILD_SCRIPT := $(shell pwd)/Android.mk