#!/usr/bin/env bash
cd jni
ndk-build clean
ndk-build
cd ..

LIBNAME=librtmp

mkdir -p ../Include/${LIBNAME}/ && cp -r *.h "$_"
mkdir -p ../Include/${LIBNAME}/ && cp -r librtmp/*.h "$_"
mkdir -p ../Libraries/Android/arm64-v8a/ && cp -r obj/local/arm64-v8a/*.a "$_"
mkdir -p ../Libraries/Android/armeabi-v7a/ && cp -r obj/local/armeabi-v7a/*.a "$_"